package com.buczel.opencv.Filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Reiz3N on 2016-04-15.
 */
public class Sobel {

    public static Mat Transform(Mat sampledImage) {

//        Bitmap resizedBitmap = null;
//
//        resizedBitmap = Bitmap.createScaledBitmap(bitmap, 28, 28, false);
//
//        Mat sampledImage = new Mat(resizedBitmap.getWidth(), resizedBitmap.getHeight(), CvType.CV_8UC1);
//        Utils.bitmapToMat(resizedBitmap, sampledImage);


        Mat blurredImage = new Mat();
        Size size = new Size(7, 7);

        Imgproc.GaussianBlur(sampledImage, blurredImage, size, 0, 0);





        Mat gray = new Mat();

        if(sampledImage.channels()>1) {
            Imgproc.cvtColor(sampledImage.clone(), gray, Imgproc.COLOR_RGB2GRAY);
        } else gray = sampledImage.clone();

        Mat xFirstDervative = new Mat(), yFirstDervative = new Mat();

        int ddepth = CvType.CV_16S;

        Imgproc.Sobel(gray, xFirstDervative, ddepth, 1, 0);
        Imgproc.Sobel(gray, yFirstDervative, ddepth, 0, 1);

        Mat absXD = new Mat(), absYD = new Mat();
        Core.convertScaleAbs(xFirstDervative, absXD);
        Core.convertScaleAbs(yFirstDervative, absYD);

        Mat edgeImage = new Mat();
        Core.addWeighted(absXD, 0.5, absYD, 0.5, 0, edgeImage);


        //displayImage(edgeImage);
        return edgeImage;
    }


}
