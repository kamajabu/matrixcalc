package com.buczel.opencv.Filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Reiz3N on 2016-04-15.
 */
public class Canny {

    public static Mat Transform(Mat sampledImage, int uno, int dos) {
//        Bitmap resizedBitmap = null;
//
//        resizedBitmap = Bitmap.createScaledBitmap(bitmap, 28, 28, false);
//
//        Mat sampledImage = new Mat(resizedBitmap.getWidth(), resizedBitmap.getHeight(), CvType.CV_8UC1);
//        Utils.bitmapToMat(resizedBitmap, sampledImage);

        Mat gray = new Mat();

        if(sampledImage.channels()>1) {
            Imgproc.cvtColor(sampledImage.clone(), gray, Imgproc.COLOR_RGB2GRAY);
        } else gray = sampledImage.clone();

        Mat edgeImage = new Mat();
        Imgproc.Canny(gray, edgeImage, uno, dos);

        return edgeImage;
    }
}
