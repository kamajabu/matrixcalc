package com.buczel.opencv.Filters;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Reiz3N on 2016-04-19.
 */
public class FloodFill {

    public static Mat Transform(Mat sampledImage, int blockSize, int c) {

        Mat result = new Mat();
        Mat result2 = new Mat();

        Mat gray = new Mat();

        if(sampledImage.channels()>1) {
            Imgproc.cvtColor(sampledImage.clone(), gray, Imgproc.COLOR_RGB2GRAY);
        } else gray = sampledImage.clone();

        Mat blurredImage = new Mat();

        Imgproc.bilateralFilter(gray, blurredImage, 3, 10, 3);

        Imgproc.erode(blurredImage, blurredImage, new Mat(3, 3, CvType.CV_8SC1));

        Imgproc.adaptiveThreshold(blurredImage, result, 255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY ,blockSize,c);


        return result;
    }
}
