package com.buczel.opencv.Filters;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by Reiz3N on 2016-04-16.
 */
public class Thresh {
    public static Mat Transform(Mat sampledImage, int blockSize, int c) {

        Mat result = new Mat();

        Mat gray = new Mat();

        if(sampledImage.channels()>1) {
            Imgproc.cvtColor(sampledImage.clone(), gray, Imgproc.COLOR_RGB2GRAY);
        } else gray = sampledImage.clone();

        Mat blurredImage = new Mat();
        Size size = new Size(7, 7);

        Imgproc.GaussianBlur(gray, blurredImage, size, 0, 0);


        Imgproc.adaptiveThreshold(blurredImage, result, 255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY ,blockSize,c);
        //Imgproc.thr

        return result;
    }
}
