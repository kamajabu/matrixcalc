package com.buczel.opencv.Backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Reiz3N on 2016-04-24.
 */
public class Database extends SQLiteOpenHelper {

    private static final String DB_NAME = "ProfileBase"; // the name of our database
    private static final int DB_VERSION = 1; // the version of the database


    public Database(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE PROFILE ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "NAME TEXT, "
                + "CLASSIFICATOR TEXT, "
                + "N0 INTEGER, "
                + "N1 INTEGER, "
                + "N2 INTEGER, "
                + "N3 INTEGER, "
                + "N4 INTEGER, "
                + "N5 INTEGER, "
                + "N6 INTEGER, "
                + "N7 INTEGER, "
                + "N8 INTEGER, "
                + "N9 INTEGER" +
                ");");

        insertData(db, "default", "default.xml", 100);
        insertData(db, "Boom", "haha", 100);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static void insertData(SQLiteDatabase db, String name, String description, int resourceId) {

        ContentValues profileValues = new ContentValues();

        profileValues.put("NAME", name);
        profileValues.put("CLASSIFICATOR", description);

        profileValues.put("N0", resourceId);
        profileValues.put("N1", resourceId);
        profileValues.put("N2", resourceId);
        profileValues.put("N3", resourceId);
        profileValues.put("N4", resourceId);
        profileValues.put("N5", resourceId);
        profileValues.put("N6", resourceId);
        profileValues.put("N7", resourceId);
        profileValues.put("N8", resourceId);
        profileValues.put("N9", resourceId);

        db.insert("PROFILE", null, profileValues);
    }
}
