package com.buczel.opencv;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class SaveImage extends Activity {

    ImageView iv;
    EditText et;
    Bitmap bmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_image);

        Bundle extras = getIntent().getExtras();
        bmp = (Bitmap) extras.getParcelable("imagebitmap");

        iv = (ImageView) findViewById(R.id.logo);
        iv.setImageBitmap(bmp);

        et = (EditText) findViewById(R.id.username);

        et.setFocusableInTouchMode(true);

        et.requestFocus();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Toast.makeText(SaveImage.this, "kurwa", Toast.LENGTH_SHORT).show();
                        saveFile();
                        SaveImage.this.finish();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return false;
            }
        });

//        et.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void afterTextChanged(Editable s) {}
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start,
//                                          int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start,
//                                      int before, int count) {
//
//
//
//            }
//        });
        //showKeyboard(getCurrentFocus());

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//
//        Bundle extras = getIntent().getExtras();
//        Bitmap bmp = (Bitmap) extras.getParcelable("imagebitmap");
//        iv.setImageBitmap(bmp );
//    }

    public void showKeyboard(View view) {
        EditText editText = (EditText) findViewById(R.id.username);
        if (editText.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

//    @Override
//    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        if (event.getKeyCode() == KeyEvent.FLAG_EDITOR_ACTION) {
//            Toast.makeText(this, "kurwa", Toast.LENGTH_SHORT).show();
//        }
//                return super.onKeyUp(keyCode, event);
//
//    }

    void saveFile() {
        FileOutputStream outStream = null;

//            // Write to SD Card
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/req_images/" + et.getText().toString());
            myDir.mkdirs();

            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);

            String fname = n + ".jpg";
            File file = new File(myDir, fname);
            Log.i("zapis", "" + file);

            if (file.exists()) file.delete();

            try {
                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

                refreshGallery(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }
}
