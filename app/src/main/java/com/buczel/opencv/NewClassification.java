package com.buczel.opencv;

import android.os.Environment;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.TermCriteria;
import org.opencv.highgui.Highgui;
import org.opencv.ml.CvSVM;
import org.opencv.ml.CvSVMParams;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Reiz3N on 2016-04-24.
 */
public class NewClassification {

    //Configure
    private static final Boolean generate = false;
    private static final String name = "personal.xml";

    private static final Logger LOGGER = Logger.getLogger("OCR");

    private static CvSVM classifier;

    private static int numbers[] = new int[10];


    static ArrayList<String> trainingFilenames = new ArrayList<String>();
    static ArrayList<Integer> labels = new ArrayList<Integer>();

    public NewClassification(){
        getTrainingFiles();
        generateClassifier();
    }

    public static void getTrainingFiles() {

        String root = Environment.getExternalStorageDirectory().toString();

        // wyszukujemy folder z plikami do uczelnia
        File folder = new File(root + "/local_training");
        // wypisujemy wszystkie foldery jakie zawiera (sa to foldery z wzorcami
        // liczb 0, 1, 2... )
        File[] listOfFiles = folder.listFiles();
        // iterujemy po folderach
        for (File file : listOfFiles) {
            // sciagamy nazwe folderu ktory aktualnie obslugujemy
            String numbersDirName = file.getName();
            // jesli nie jest to .DS_Store
            if (!numbersDirName.contains(".DS_Store")) {

                // sciagamy do inta nazwe aktualnego foldera
                int currentLabel = Integer.valueOf(numbersDirName);

                File folderImages = new File(file.getAbsolutePath());
                File[] listOfImages = folderImages.listFiles();
                //count element for database
                numbers[currentLabel] = listOfImages.length;
                // iterujemy po zawartosci folderu
                for (File imageFile : listOfImages) {
                    if (!imageFile.getName().contains(".DS_Store")) {
                        String trainingJpgFileName = imageFile.getPath();
                        // TODO zmien absolutna na relatywna
                        // zapisujemy bezposrednia sciezke
                        trainingFilenames.add(trainingJpgFileName);
                        // oraz jakiej cyfrze odpowiada
                        labels.add(currentLabel);

                    }
                }

            }
        }

    }

    public static void generateClassifier() {

        // image dimensions
        int imgArea = 28 * 28;

        // process images

        // tworzymy ciag uczacy
        Mat labelsMat = new Mat(labels.size(), 1, CvType.CV_32S);


        // will hold training data
        Mat trainingMat = new Mat(trainingFilenames.size(), imgArea,
                CvType.CV_32FC1);

        // loop over training files
        for (int index = 0; index < trainingFilenames.size(); index++) {

            // read image file (grayscale)
            Mat imgMat = Highgui.imread(trainingFilenames.get(index), 0);

            int ii = 0; // Current column in training_mat

            // process individual pixels and add to our training material
            for (int i = 0; i < imgMat.rows(); i++) {
                for (int j = 0; j < imgMat.cols(); j++) {
                    trainingMat.put(index, ii++, imgMat.get(i, j));
                    labelsMat.put(index, 0, labels.get(index));
                }
            }

            imgMat.release();
        }
        /**
         * Train
         **/

        train(trainingMat, labelsMat);


       }

    public static void train(Mat trainingMat, Mat labelsMat){

        TermCriteria criteria = new TermCriteria(TermCriteria.MAX_ITER, 100, 1e-6);
        CvSVMParams params = new CvSVMParams();
        params.set_kernel_type( CvSVM.POLY );
        params.set_svm_type( CvSVM.C_SVC );
        params.set_gamma( 3 );
        params.set_degree(3);
        params.set_term_crit(criteria);

        classifier = new CvSVM(trainingMat,labelsMat, new Mat(), new Mat(), params);

        classifier.save(Environment.getExternalStorageDirectory().getAbsolutePath()+"/" + name);

        trainingMat.release();
        labelsMat.release();
    }
}
