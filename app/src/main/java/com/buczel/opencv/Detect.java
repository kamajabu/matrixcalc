package com.buczel.opencv;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;

import java.io.InputStream;

/**
 * Created by Reiz3N on 2016-04-15.
 */
public class Detect {

    static private CvSVM classifier;

    static{
        classifier = new CvSVM();

        try {
            classifier.load(Environment.getExternalStorageDirectory().getAbsolutePath() + "/klaspersonal.xml");
        } catch (Exception e) {
            Log.e("Classifier", "Can't load classifier!");
            e.printStackTrace();
        }
    }

    public static int Analize(InputStream istr) {

        Bitmap bitmap = null, resizedBitmap = null;

        bitmap = BitmapFactory.decodeStream(istr);
        resizedBitmap = Bitmap.createScaledBitmap(bitmap, 28, 28, false);

        Mat img2D = new Mat(resizedBitmap.getWidth(), resizedBitmap.getHeight(), CvType.CV_8UC1);
        Utils.bitmapToMat(resizedBitmap, img2D);
        Imgproc.cvtColor(img2D, img2D, Imgproc.COLOR_RGB2GRAY);


        //convert 2d to 1d
        Mat testMat = img2D.clone().reshape(1, 1);
        testMat.convertTo(testMat, CvType.CV_32F);

        // try to predict which number has been drawn
        try {
            float predicted = classifier.predict(testMat);

            Log.i("Odpowiedz", "Test liczba:" + predicted);

            return (int) predicted;

        } catch (Exception ex) {
            return -1;
        }


    }

    public static int AnalizeMat(Mat mat) {
        Mat testMat = mat.clone().reshape(1, 1);
        testMat.convertTo(testMat, CvType.CV_32F);

        // try to predict which number has been drawn
        try {
            float predicted = classifier.predict(testMat);
            testMat.release();

            //Log.i("Odpowiedz", "-> CAnny -> Test liczba:" + predicted);

            return (int) predicted;

        } catch (Exception ex) {
            testMat.release();
            return -1;
        }

    }

}
