package com.buczel.opencv;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by Reiz3N on 2016-04-15.
 */
public class OCRMain {
    private static final Logger LOGGER = Logger.getLogger("OCR");

    static ArrayList<String> trainingFilenames = new ArrayList<String>();
    static ArrayList<Integer> labels = new ArrayList<Integer>();
    static Context myContext;

    protected static final String XML = "data/test.xml";
    protected static final String FILE_TEST = "data/negativo/1.jpg";

    private static CvSVM clasificador;



    static {
        //System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
//        trainingImages = new Mat();
//        trainingLabels = new Mat();
//        trainingData = new Mat();
//        classes = new Mat();
    }

    public OCRMain(Context context){
        this.myContext = context;
    }

    public static void conversion() {



        getTrainingFiles();

        // image dimensions
        int imgArea = 28 * 28;


        //
        // process images
        // tworzymy ciag uczacy
        Mat labelsMat = new Mat(labels.size(), 1, CvType.CV_32S);


        // will hold training data
        Mat trainingMat = new Mat(trainingFilenames.size(), imgArea, CvType.CV_32FC1);

        AssetManager assetManager = myContext.getAssets();

        int progress = 0;
//         loop over training files
        for (int index = 0; index < trainingFilenames.size(); index++) {

            // read image file (grayscale)

            InputStream istr;
            Bitmap bitmap = null;
            try {
                istr = assetManager.open(trainingFilenames.get(index));
                bitmap = BitmapFactory.decodeStream(istr);
            } catch (IOException e) {
                // handle exception
            }
            Mat imgMat = new Mat(bitmap.getWidth(), bitmap.getHeight(), CvType.CV_8UC1);
            Utils.bitmapToMat(bitmap, imgMat);
            Imgproc.cvtColor(imgMat, imgMat, Imgproc.COLOR_RGB2GRAY);


            int ii = 0; // Current column in training_mat

            // process individual pixels and add to our training material
            for (int i = 0; i < imgMat.rows(); i++) {
                for (int j = 0; j < imgMat.cols(); j++) {
                    trainingMat.put(index, ii++, imgMat.get(i, j));
                    labelsMat.put(index, 0, labels.get(index));
                }
            }
            if(index%80==0){
                progress++;
                Log.d("Progress", "Postep wczytywania macierzy uczacej wynosi: " + progress);
            }

        }



        /**
         * Train
         **/
//REGION make new classifier
//        TermCriteria criteria = new TermCriteria(TermCriteria.MAX_ITER, 100, 1e-6);
//        CvSVMParams params = new CvSVMParams();
//        params.set_kernel_type( CvSVM.POLY );
//        params.set_svm_type( CvSVM.C_SVC );
//        params.set_gamma( 3 );
//        params.set_degree(3);
//        params.set_term_crit(criteria);
//
//        CvSVM classifier = new CvSVM(trainingMat,labelsMat, new Mat(), new Mat(), params);
//
//        classifier.save(Environment.getExternalStorageDirectory().getAbsolutePath()+"/klas60k.xml");
//        classifier.save(Environment.getExternalStorageDirectory().getAbsolutePath()+"/klas60k.yml");
//
//        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/klas60k.yml");
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        mediaScanIntent.setData(Uri.fromFile(file));
//        myContext.sendBroadcast(mediaScanIntent);
//
//        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/klas60k.xml");
//        mediaScanIntent.setData(Uri.fromFile(file));
//        myContext.sendBroadcast(mediaScanIntent);



//REGION load
//        CvSVM classifier = new CvSVM();
//        classifier.load(Environment.getExternalStorageDirectory().getAbsolutePath()+"/klas.xml");
//
//        InputStream istr;
//        Bitmap bitmap = null, resizedBitmap = null;
//        try {
//            istr = assetManager.open("test.jpg");
//            bitmap = BitmapFactory.decodeStream(istr);
//            resizedBitmap = Bitmap.createScaledBitmap(bitmap, 28, 28, false);
//        } catch (IOException e) {
//            // handle exception
//        }
//        Mat img2D = new Mat(resizedBitmap.getWidth(), resizedBitmap.getHeight(), CvType.CV_8UC1);
//        Utils.bitmapToMat(resizedBitmap, img2D);
//        Imgproc.cvtColor(img2D, img2D, Imgproc.COLOR_RGB2GRAY);
//
//
//
//        //convert 2d to 1d
//        Mat testMat = img2D.clone().reshape(1, 1);
//        testMat.convertTo(testMat, CvType.CV_32F);
//
//        // try to predict which number has been drawn
//        try{
//            float predicted = classifier.predict(testMat);
//
//            Log.i("Odpowiedz", "Test liczba:" + predicted);
//
//        }catch(Exception ex){
//
//        }

    }

    public static void getTrainingFiles() {

        AssetFileDescriptor istr;
        AssetManager assetManager = myContext.getAssets();

        String path = "training_files";
        String [] list;
        try {
            list = myContext.getAssets().list(path);
            if (list.length > 0) {
                // This is a folder
                for (String file : list) {
                    if(!file.contains(".DS_Store")) {
                        // sciagamy do inta nazwe aktualnego foldera
                        int currentLabel = Integer.valueOf(file);


                        String[] listImages;
                        listImages = myContext.getAssets().list(path + "/" + file);
                        for (String imageFile : listImages) {
                            if (!file.contains(".DS_Store")) {
                                String trainingJpgFileName = path + '/'+ file + '/' + imageFile;

                                // zapisujemy bezposrednia sciezke
                                trainingFilenames.add(trainingJpgFileName);
                                // oraz jakiej cyfrze odpowiada
                                labels.add(currentLabel);

                            }
                        }

                    }
                }
            }
        } catch (IOException e) {

        }

    }

//    public static void Test(){
//        // read image file (grayscale)
//        //Mat img2D = Imgcodecs.imread(trainingFilenames.get(0), 0);
//        Mat img2D = Highgui.imread("test.jpg", 0);
//        //convert 2d to 1d
//        Mat testMat = img2D.clone().reshape(1, 1);
//        testMat.convertTo(testMat, CvType.CV_32F);
//
//        // try to predict which number has been drawn
//        try{
//            float predicted = classifier.predict(testMat);
//
//            LOGGER.info("Recognizing following number -> " + predicted + '\n');
//
//        }catch(Exception ex){
//
//        }
//
//        Mat in = Highgui.imread( new File( FILE_TEST ).getAbsolutePath(), Highgui.CV_LOAD_IMAGE_GRAYSCALE );
//        clasificador.load( new File( XML ).getAbsolutePath() );
//        System.out.println( clasificador );
//        Mat out = new Mat();
//        in.convertTo( out, CvType.CV_32FC1 );
//        out = out.reshape( 1, 1 );
//        System.out.println( out );
//        System.out.println( clasificador.predict( out ) );
//    }



}

