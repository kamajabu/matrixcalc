package com.buczel.opencv.Menu;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.buczel.opencv.Backend.Database;
import com.buczel.opencv.MainActivity;
import com.buczel.opencv.R;

public class ProfileListActivity extends Activity {

    private SQLiteDatabase db;
    private Cursor favoritesCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_list);

        //Create an OnItemClickListener for the Options ListView
        AdapterView.OnItemClickListener itemClickListener =
                new AdapterView.OnItemClickListener(){
                    public void onItemClick(AdapterView<?> listView,
                                            View v,
                                            int position,
                                            long id) {
                        if (position == 0) {
                            //TODO
                            Intent intent = new Intent(ProfileListActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }
                };

        //Add the listener to the Options ListView

        //Populate the list_favorites ListView from a cursor
        ListView listFavorites = (ListView)findViewById(R.id.list_favorites);

        try{
            SQLiteOpenHelper starbuzzDatabaseHelper = new Database(this);
            db = starbuzzDatabaseHelper.getReadableDatabase();

            favoritesCursor = db.query("PROFILE",
                    new String[] { "_id", "NAME"},
                    null, null, null, null, null);
            //Use the cursor in the cursor adapter.
            //Display the names of the profiles in the ListView.


            CursorAdapter favoriteAdapter = new SimpleCursorAdapter(ProfileListActivity.this,
                            android.R.layout.simple_list_item_1,
                            favoritesCursor,
                            new String[]{"NAME"},
                            new int[]{android.R.id.text1});
            listFavorites.setAdapter(favoriteAdapter);

            favoriteAdapter.notifyDataSetChanged();

        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }


//        //Navigate to DrinkActivity if a drink is clicked
//        listFavorites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> listView, View v, int position, long id)
//            {
//                Intent intent = new Intent(TopLevelActivity.this, DrinkActivity.class);
//                intent.putExtra(DrinkActivity.EXTRA_DRINKNO, (int)id);
//                startActivity(intent);
//            }
//        });
    }
    //Close the cursor and database in the onDestroy() method
    @Override
    public void onDestroy(){
        super.onDestroy();
        favoritesCursor.close();
        db.close();
    }
}