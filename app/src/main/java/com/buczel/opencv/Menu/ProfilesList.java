package com.buczel.opencv.Menu;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.buczel.opencv.Backend.Database;

/**
 * Created by Reiz3N on 2016-04-24.
 */
public class ProfilesList extends Activity{

   public void hm(int profileNumber){
      try {
         SQLiteOpenHelper starbuzzDatabaseHelper = new Database(this);
         SQLiteDatabase db = starbuzzDatabaseHelper.getWritableDatabase();
         Cursor cursor = db.query("OCRprofiles",
                 new String[]{"NAME", "CLASSIFICATOR", "N0", "N1", "N2", "N3", "N4", "N5", "N6", "N7", "N8", "N9"},
                 "_id = ?",
                 new String[]{Integer.toString(profileNumber)},
                 null, null, null);


         //Move to the first record in the Cursor
         if (cursor.moveToFirst()) {
            //Get the details from the cursor
            String nameText = cursor.getString(0);
            String classificatorText = cursor.getString(1);

            int count_0 = cursor.getInt(2);
            int count_1 = cursor.getInt(3);
            int count_2 = cursor.getInt(4);
            int count_3 = cursor.getInt(5);
            int count_4 = cursor.getInt(6);
            int count_5 = cursor.getInt(7);
            int count_6 = cursor.getInt(8);
            int count_7 = cursor.getInt(9);
            int count_8 = cursor.getInt(10);
            int count_9 = cursor.getInt(11);
         };
         cursor.close();
         db.close();
      } catch(SQLiteException e) {
         Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
         toast.show();
      }
   }

   public void databaseUpdate(int number, int numberCount, int profileNumber){

      ContentValues profileValues = new ContentValues();

      profileValues.put("_"+ number, numberCount);
      SQLiteOpenHelper starbuzzDatabaseHelper = new Database(this);
      try {
         SQLiteDatabase db = starbuzzDatabaseHelper.getWritableDatabase();
         db.update("OCRprofiles", profileValues,
                 "_id = ?",
                 new String[] {Integer.toString(profileNumber)});

         db.close();

      } catch(SQLiteException e) {
         Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
         toast.show();
      }
   }
}

