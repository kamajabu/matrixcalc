package com.buczel.opencv;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.buczel.opencv.Menu.ProfileListActivity;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "HelloVisionWorld";

    private Mat mRgba;
    private Mat mGray;
    public boolean takePhoto = false;
    AssetManager assetManager;
    private Mat mainMat, mainMatPreview, mainMatAnalize, roiMat, background;

    private int expectedWidth = 200;
    private int expectedHeight = 200;

    private SeekBar volumeControl = null;
    ImageView iv = null;


    private int width, height;

    TextView t1, t2, t3, t4, t5, t6, t7;

    //A class used to implement the interaction between OpenCV and the
//device camera.
    private CameraBridgeViewBase mOpenCvCameraView;

    //This is the callback object used when we initialize the OpenCV
//library asynchronously
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

        @Override
//This is the callback method called once the OpenCV //manageris connected
        public void onManagerConnected(int status) {
            switch (status) {
                //Once the OpenCV manager is successfully connected we can enable the
                //camera interaction with the defined OpenCV camera view
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    work();

                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);



        Log.i(TAG, "called onCreate");

        assetManager = getAssets();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloVisionView);
        //Set the view as visible
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        //Register your activity as the callback object to handle
        // cameraframes
        mOpenCvCameraView.setCvCameraViewListener(this);


        Button captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!takePhoto) takePhoto = true;
                    }
                }
        );




        Button saveButton = (Button) findViewById(R.id.button_save);
        saveButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Bitmap bitMap = Bitmap.createBitmap(background.cols(), background.rows(), Bitmap.Config.RGB_565);
                        // convert to bitmap:
                        Utils.matToBitmap(background, bitMap);

                        Bundle extras = new Bundle();
                        extras.putParcelable("imagebitmap", bitMap);


                        Intent intent = new Intent(MainActivity.this, SaveImage.class);
                        intent.putExtras(extras);

                        startActivity(intent);
                    }
                }
        );

        volumeControl = (SeekBar) findViewById(R.id.seek1);

        volumeControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {

                if (progressChanged > 0) {
                    double progressPercent = progressChanged * .01;
                    int tempExpectedWidth = (int) (200 * progressPercent);
                    if (tempExpectedWidth > 27) {
                        expectedWidth = tempExpectedWidth;
                        expectedHeight = expectedWidth;

                        Toast.makeText(MainActivity.this, "Size: " + expectedWidth,
                                Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });

        t1 = (TextView) findViewById(R.id.textView1);
        t2 = (TextView) findViewById(R.id.textView2);
        t3 = (TextView) findViewById(R.id.textView3);
        t4 = (TextView) findViewById(R.id.textView4);
        t5 = (TextView) findViewById(R.id.textView5);
        t6 = (TextView) findViewById(R.id.textView6);
        t7 = (TextView) findViewById(R.id.textView7);

//        OCRMain ocrMain = new OCRMain(getApplicationContext());
//        ocrMain.conversion();
        //TODO
//        new NewClassification();


    }

//    private void showEditDialog() {
//        FragmentManager fm = getSupportFragmentManager();
//        LearningDialogFragment editNameDialog = new LearningDialogFragment();
//        editNameDialog.show(fm, "fragment_edit_name");
//    }

    @Override
    public void onResume() {
        super.onResume();
        //Call the async initialization and pass the callback object we
        //created later, and chose which version of OpenCV library to
        // load. Just make sure that the OpenCV manager you installed
        // supports the version you are trying to load.
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }


    @Override
    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    @Override
    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        System.gc();
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();
        //We're returning the colored frame as is to be rendered on
        //thescreen.

        Mat temp = inputFrame.rgba().clone();
        width = temp.cols() / 2 - expectedWidth / 2;
        height = temp.rows() / 2 - expectedHeight / 2;


        if (takePhoto) {


            //you have to use clone or it will copy part that gets edited later

            //cutting screen
            mainMat = temp.submat(new Rect(width, height, expectedWidth, expectedHeight));
            temp.release();



            //version for grey
            Mat tempGrey = inputFrame.gray().clone();
            Mat mainMatGrey = tempGrey.submat(new Rect(width, height, expectedWidth, expectedHeight));
            tempGrey.release();

            //version with transform before cutting off zone
            //mainMatAnalize = Thresh.Transform(mainMat.clone(),11, 2);





            //if you don't use clone it will work on the same matrix
            mainMatPreview = inputFrame.rgba();

            //version with transform after cutting out zone
            //displayImage(mainMatGrey.clone(), 6);
            findContours(mainMatGrey.clone());
            mainMatGrey.release();




            work();



            takePhoto = false;
        } temp.release();
        if(mainMat!=null) drawZone();
        return mRgba;
    }

    public void drawZone() {
        Rect roi = new Rect(width, height, expectedWidth, expectedHeight);
        roiMat = mRgba.submat(roi);

        Mat color = new Mat(roi.size(), CvType.CV_8UC4, new Scalar(0, 125, 125));
        double alpha = 0.3;

        Core.addWeighted(color, alpha, roiMat, 1.0 - alpha, 0.0, roiMat);



    }

    private void work() {

//        try {
//            InputStream istr;
//            istr = assetManager.open("test.jpg");
//            Detect.Analize(istr);
//            istr = assetManager.open("test2.jpg");
//            Detect.Analize(istr);
//            istr = assetManager.open("test3.jpg");
//            Detect.Analize(istr);
//            istr = assetManager.open("test4.jpg");
//            Detect.Analize(istr);
//            takePhoto = false;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        if (mainMat != null) {
            //------

            //resize image to size that classificator is teached on
            Mat sampledImage = new Mat();
            double sampleRatio = calculateSubSampleSize(mainMat, 28, 28);
            Imgproc.resize(mainMat, sampledImage, new Size(), sampleRatio, sampleRatio, Imgproc.INTER_AREA);
            mainMat = sampledImage;


                displayImage(mainMat, 1);

                //displayImage(Sobel.Transform(mainMat), 2);

                //displayImage(Canny.Transform(mainMat, 100, 200), 3);

                //displayImage(Thresh.Transform(mainMat, 5, 1), 4);

                //displayImage(Thresh.Transform(mainMat, 11, 2), 5);

                //displayImage(Thresh.Transform(mainMat, 21, 6), 6);
        }


    }

    private void displayImage(final Mat image, final int number) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Mat gray = new Mat();
                // create a bitMap
                Bitmap bitMap = Bitmap.createBitmap(image.cols(), image.rows(), Bitmap.Config.RGB_565);
                // convert to bitmap:
                Utils.matToBitmap(image, bitMap);
                // find the imageview and draw it!

                switch (number) {
                    case 1:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView1);


                        if(image.channels()>1) {
                            Imgproc.cvtColor(image.clone(), gray, Imgproc.COLOR_RGB2GRAY);
                        } else gray = image.clone();

                        t1.setText(String.valueOf(Detect.AnalizeMat(gray)));

                        Utils.matToBitmap(gray, bitMap);


                        break;
                    case 2:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView2);
                        t2.setText(String.valueOf(Detect.AnalizeMat(image)));
                        break;
                    case 3:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView3);

                        t3.setText(String.valueOf(Detect.AnalizeMat(image)));
                        break;
                    case 4:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView4);
                        t4.setText(String.valueOf(Detect.AnalizeMat(image)));
                        break;
                    case 5:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView5);
                        t5.setText(String.valueOf(Detect.AnalizeMat(image)));
                        break;
                    case 6:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView6);
                        t6.setText(String.valueOf(Detect.AnalizeMat(image)));
                        break;
                    case 7:
                        iv = (ImageView) findViewById(R.id.IODarkRoomImageView7);
                        t7.setText(String.valueOf(Detect.AnalizeMat(image)));
                        break;
                }

                gray.release();
                iv.setImageBitmap(bitMap);
//stuff that updates ui

            }
        });

    }

    private static double calculateSubSampleSize(Mat srcImage, int reqWidth,
                                                 int reqHeight) {
// Raw height and width of image
        final int height = srcImage.height();
        final int width = srcImage.width();
        double inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
// Calculate ratios of requested height and width to the raw
//height and width
            final double heightRatio = (double) reqHeight / (double) height;
            final double widthRatio = (double) reqWidth / (double) width;
// Choose the smallest ratio as inSampleSize value, this will
//guarantee final image with both dimensions larger than or
//equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        } else {
            final double heightRatio = (double) reqHeight / (double) height;
            final double widthRatio = (double) reqWidth / (double) width;
// Choose the smallest ratio as inSampleSize value, this will
//guarantee final image with both dimensions larger than or
//equal to the requested height and width.

            //28x28, 20x10 28/20 and 28/10
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    private void findContours(Mat analizeMat) {
        Mat greyAnalizeMat = new Mat();

//
//        Mat resizedImage = new Mat();
//        double sampleRatio = calculateSubSampleSize(analizeMat, 28, 28);
//        Imgproc.resize(analizeMat, resizedImage, new Size(), sampleRatio, sampleRatio, Imgproc.INTER_AREA);
//        analizeMat = resizedImage;

//        if(analizeMat.channels()>1) {
//                        Imgproc.cvtColor(analizeMat.clone(), greyAnalizeMat, Imgproc.COLOR_RGB2GRAY, 1);
//                        //greyAnalizeMat = analizeMat.clone().reshape(1, 28);
//        } else greyAnalizeMat = analizeMat.clone();
        greyAnalizeMat = transform(analizeMat.clone(), 3, 2);
        //displayImage(analizeMat, 4);
//        analizeMat = Canny.Transform(analizeMat, 100, 200);
//        analizeMat = Thresh.Transform(analizeMat, 21, 11);
        //displayImage(analizeMat, 5);
        //displayImage(greyAnalizeMat, 7);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.findContours(greyAnalizeMat.clone(), contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
        double biggestHeight = 0;
        int biggestHeightIndex = 0;
        for (int i = 0; i < contours.size(); i++) {
            //System.out.println(Imgproc.contourArea(contours.get(i)));
            if (Imgproc.contourArea(contours.get(i)) > 50) {


                Rect rect = Imgproc.boundingRect(contours.get(i));
                //System.out.println(rect.height);
                if (rect.height > 10) {
                    //draw rectangle
                    Core.rectangle(mainMatPreview, new Point(rect.x+width, rect.y+height), new Point(rect.x + rect.width+width, rect.y + rect.height+height), new Scalar(0, 0, 255));
                    if(rect.height>biggestHeight) {
                        biggestHeightIndex = i;
                        biggestHeight=rect.height;
                    }
                }
            }
        }
        if(contours.size()!=0 && biggestHeight!=0) {
            //We take coords of biggest found rect
            Rect rect = Imgproc.boundingRect(contours.get(biggestHeightIndex));
            //Cut off this rectangle and its content
            Mat cutMat = greyAnalizeMat.clone().submat(rect);
            //Display it
            //displayImage(cutMat, 7);
            //We count ratio we need to apply to resize in to native learning data
            double ratio = calculateSubSampleSize(cutMat, 22, 22);
            //we create background
            Rect backgroundRect = new Rect(0, 0, 28, 28);

            Mat sampledImage = new Mat();

            //Resize content to native learning data size using ratio counted earlier
            Imgproc.resize(cutMat, sampledImage, new Size(), ratio, ratio, Imgproc.INTER_AREA);

            //Imgproc.bilateralFilter(sampledImage, sampledImage, 3, 5, 3);
            Imgproc.medianBlur(sampledImage, sampledImage, 1);

            //create black Matrix in size of background
            background = new Mat(backgroundRect.size(), CvType.CV_8UC1, new Scalar(0, 0, 0));


            //Core.addWeighted(sampledImage, alpha, background, 1.0 - alpha, 0.0, background);
            //sampledImage.copyTo(background);

            //small_image.copyTo(big_image(cv::Rect(x,y,small_image.cols, small_image.rows)));
            sampledImage.copyTo(background.submat(new Rect(background.cols()/2 - sampledImage.cols()/2,
                                                    3,
                                                    sampledImage.cols(),
                                                    sampledImage.rows())));

            displayImage(background, 7);


        }



        biggestHeight = 0;
        biggestHeightIndex = 0;
    }


    public Mat transform(Mat sampledImage, int blockSize, int c) {

        Mat result = new Mat();
        Mat result2 = new Mat();

        Mat gray = new Mat();

        if(sampledImage.channels()>1) {
            Imgproc.cvtColor(sampledImage.clone(), gray, Imgproc.COLOR_RGB2GRAY);
        } else gray = sampledImage.clone();

        //displayImage(gray.clone(), 2);

        Mat blurredImage = new Mat();

        Imgproc.bilateralFilter(gray, blurredImage, 10, 50, 30);

        //displayImage(blurredImage.clone(), 3);

        Imgproc.erode(blurredImage, blurredImage, new Mat(7, 7, CvType.CV_8UC1));

        //displayImage(blurredImage.clone(), 4);

        //Imgproc.THRESH_BINARY black numbers on white backround
        //Imgproc.THRESH_BINARY_INV white numbers on black background

        Imgproc.adaptiveThreshold(blurredImage, result, 255,Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV ,21, 2);

        //displayImage(result.clone(), 5);

        Mat resizedImage = new Mat();
        double sampleRatio = calculateSubSampleSize(result, 28, 28);
        Imgproc.resize(result, resizedImage, new Size(), sampleRatio, sampleRatio, Imgproc.INTER_AREA);
        result = resizedImage;

        displayImage(result.clone(), 6);

        return result;
    }


}